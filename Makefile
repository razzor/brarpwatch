CFLAGS := -O2 -pipe -Wall
LDFLAGS := -lpthread -lnfnetlink -lnetfilter_log

OBJS := configfile.o datastore.o event.o logging.o netlink.o ulogparse.o

all: brarpwatch

brarpwatch: $(OBJS) brarpwatch.o
	$(CC) $(CFLAGS) $(LDFLAGS) $^ -o $@

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

%.d: %.c
	$(CC) $(CFLAGS) -MM -c $< -o $@

clean:
	rm -f brarpwatch *.d *.o *.log

DEPS := $(wildcard *.c)
-include $(DEPS:.c=.d)
