/***************************************************************************
 *   Copyright (C) 07/2007 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; version 2 of the License                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdlib.h>
#include <stdint.h>

#include <time.h>
#include <net/ethernet.h>
#include <netinet/if_ether.h>
#include <arpa/inet.h>

#include <net/if.h>
#include <linux/netfilter_bridge/ebt_ulog.h>

#include "logging.h"

#if 0
static int is_same_mac(uint8_t *a, uint8_t *b)
{
	return ((a[0] ^ b[0]) | (a[1] ^ b[1]) | (a[2] ^ b[2]) |
		(a[3] ^ b[3]) | (a[4] ^ b[4]) | (a[5] ^ b[5])) == 0x00;
}

static int is_null_mac(uint8_t *a)
{
	return (a[0] | a[1] | a[2] | a[3] | a[4] | a[5]) == 0x00;
}

static int is_broadcast_mac(uint8_t *a)
{
	return (a[0] & a[1] & a[2] & a[3] & a[4] & a[5]) == 0xFF;
}
#endif

int parse_ulog_packet(void *data)
{
	ebt_ulog_packet_msg_t *pkt = (ebt_ulog_packet_msg_t *)data;

	struct ether_header *eh = (struct ether_header *)pkt->data;
	struct ether_arp *ah = (struct ether_arp *)(eh +1);

	/* only ARP packets with ETHER <-> IPv4 */
	if (eh->ether_type != 0x0608 || ah->ea_hdr.ar_hrd != 0x0100 || ah->ea_hdr.ar_pro != 0x0008)
		return 0;

	char time_str[40];
	struct tm *ptm = localtime(&pkt->stamp.tv_sec);
	strftime(time_str, sizeof(time_str), "%Y-%m-%d %H:%M:%S", ptm);

	char *op = "unknown op ";
	if (ah->ea_hdr.ar_op == 0x0100)
		op = "ARP Request";

	else if (ah->ea_hdr.ar_op == 0x0200)
		op = "ARP Reply  ";

	char sip[16], dip[16];
	inet_ntop(AF_INET, ah->arp_spa, sip, sizeof(sip));
	inet_ntop(AF_INET, ah->arp_tpa, dip, sizeof(dip));

	log_print(LOG_DEBUG, "%s: %s(%s): %02x:%02x:%02x:%02x:%02x:%02x => %02x:%02x:%02x:%02x:%02x:%02x %s %02x:%02x:%02x:%02x:%02x:%02x (%s) => %02x:%02x:%02x:%02x:%02x:%02x (%s)",
		time_str, pkt->indev, pkt->physindev,
		eh->ether_shost[0], eh->ether_shost[1], eh->ether_shost[2],
		eh->ether_shost[3], eh->ether_shost[4], eh->ether_shost[5],
		eh->ether_dhost[0], eh->ether_dhost[1], eh->ether_dhost[2],
		eh->ether_dhost[3], eh->ether_dhost[4], eh->ether_dhost[5],
		op,
		ah->arp_sha[0], ah->arp_sha[1], ah->arp_sha[2],
		ah->arp_sha[3], ah->arp_sha[4], ah->arp_sha[5], sip,
		ah->arp_tha[0], ah->arp_tha[1], ah->arp_tha[2],
		ah->arp_tha[3], ah->arp_tha[4], ah->arp_tha[5], dip);

	return 0;
}
