#ifndef _NETLINK_H_
#define _NETLINK_H_

int netlink_init(void);
void netlink_close(void);

#endif /* _NETLINK_H_ */
