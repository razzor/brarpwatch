#ifndef _DATASTORE_H_
#define _DATASTORE_H_

#include <stdint.h>

int datastore_init(void);
void datastore_close(void);

int ds_check_update_ip(const char *dev, uint32_t ip, uint8_t *mac, uint32_t timestamp);

#endif /* _DATASTORE_H_ */
