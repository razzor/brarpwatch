/***************************************************************************
 *   Copyright (C) 07/2007 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; version 2 of the License                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>

#include <time.h>

#include "configfile.h"
#include "datastore.h"
#include "event.h"
#include "logging.h"
#include "netlink.h"

#define DEFAULT_CONFIG "brarpwatch.conf"
#define DEFAULT_LOGFILE "brarpwatch.log"

static struct option opts[] = {
	{"config",	1, 0, 'c'},
	{"debug",	0, 0, 'd'},
	{"help",	0, 0, 'h'},
	{0, 0, 0, 0}
};

int main(int argc, char *argv[])
{
	char *config = DEFAULT_CONFIG;
	int code, arg = 0, debug = 0;

	do {
		code = getopt_long(argc, argv, "c:dh", opts, &arg);

		switch (code) {
		case 'c':	/* config */
				config = optarg;
				break;

		case 'd':	/* debug */
				debug = 1;
				break;

		case 'h':	/* help */
				printf("Usage: brarpwatch [options]\n"
					"Options: \n"
					"  --config       -c  configfile  use this configfile\n"
					"  --debug        -d              do not fork and log to stderr\n"
					"  --help         -h              this help\n"
					"\n");
				exit(0);
				break;

		case '?':	/* error */
				exit(-1);
				break;

		default:	/* unknown / all options parsed */
				break;
		}
	} while (code != -1);

	/* parse config file */
	if (config_parse(config))
		exit(1);

	if (datastore_init() < 0)
		exit(1);

	if (netlink_init() < 0) {
		datastore_close();
		exit(1);
	}

#if 0
	unsigned char mac[6];
	unsigned char mac2[6];
	unsigned char mac3[6];
	unsigned int ip = 0x01000a0a;
	unsigned int ip2 = 0x02000a0a;
	long stamp = time(NULL);

	// subnet not found
	ds_check_update_ip("eth0", ip +1, mac, stamp++);

	// wrong dev not found
	ds_check_update_ip("eth1", ip, mac, stamp++);

	// ok
	ds_check_update_ip("eth0", ip, mac, stamp++);

	// wrong dev
	ds_check_update_ip("eth1", ip, mac, stamp++);

	// update
	ds_check_update_ip("eth0", ip, mac, stamp++);

	// warn
	ds_check_update_ip("eth0", ip, mac2, stamp++);

	// warn
	ds_check_update_ip("eth0", ip, mac3, stamp++);

	// ok
	ds_check_update_ip("eth0", ip2, mac, stamp++);

#endif

	event_loop();

	netlink_close();
	datastore_close();
	return 0;
}
